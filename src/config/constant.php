<?php
return [
	"status" => [
		0 => "Disable",
		1 => "Enable"
	],
	"roles" => [
		1909 => "Administration",
		1 => "Accountant",
		2 => "Editor",
		3 => "Users"
	]
];
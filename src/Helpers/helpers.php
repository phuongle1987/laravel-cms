<?php
/**
 * This is helpers include all function
 */
if (!function_exists('checkEmailSpan'))
{
  function checkEmailSpan($email)
  {
    $error = array(
      '@yopmail.com',
      '@yopmail.fr',
      '@yopmail.net',
      '@cool.fr.nf',
      '@jetable.fr.nf',
      '@nospam.ze.tc',
      '@nomail.xl.cx',
      '@mega.zik.dj',
      '@speed.1s.fr',
      '@courriel.fr.nf',
      '@moncourrier.fr.nf',
      '@monemail.fr.nf',
      '@monmail.fr.nf',
      '@armyspy.com',
      '@cuvox.de',
      '@dayrep.com',
      '@einrot.com',
      '@fleckens.hu',
      '@gustr.com',
      '@jourrapide.com',
      '@rhyta.com',
      '@superrito.com',
      '@teleworm.us',
      '@mailinator.com',
      '@spam4.me'
    );
    $check = strstr($email, "@");
    if(in_array($check, $error))
      return false;
    return $email;
  }
}

if (!function_exists('putUploadImage')){
    function putUploadImage($image, $filename, $default = false) {
      $small = array(120,120);
      $thumb = array(350,350);
      $image = file_get_contents($image);
      $path = "public/media/".$filename;
      if (!is_dir(storage_path("public/media/")))
        \Storage::makeDirectory("public/media/");
      \Storage::put(
        $path, $image, 'public'
      );
      if ($default == true) {
        if (!is_dir(storage_path("public/media/thumb/")))
          \Storage::makeDirectory("public/media/thumb/");
        $pathThumb = "public/media/thumb/".$filename;
        \Storage::put(
          $pathThumb, $image, 'public'
        );
        thumbnailImage($thumb, $pathThumb);
        if (!is_dir(storage_path("public/media/small/")))
          \Storage::makeDirectory("public/media/small/");
        $pathSmall = "public/media/small/".$filename;
        \Storage::put(
          $pathSmall, $image, 'public'
        );
        thumbnailImage($small, $pathSmall);
      }
      return $path;
    }
  }

  if (!function_exists('deleteImage')) {
    function deleteImage($path, $default = true) {
      if ($default == true) {
        $thumb = str_replace("media/","media/thumb/",$path);
        $small = str_replace("media/","media/small/",$path);
        \Storage::delete($thumb);
        \Storage::delete($small);
      }
      return \Storage::delete($path);
    }
  }

  if (!function_exists('getImage')) {
    function getImage($path){
      return \Storage::url($path);
    }
  }

  if (!function_exists('getImageThumb'))
  {
    function getImageThumb($path, $size = "thumb")
    {
      $path = str_replace("media/","media/$size/",$path);
      return \Storage::url($path);
    }
  }

if (!function_exists('thumbnailImage'))
{
    function thumbnailImage($size = array(200,200), $thumb)
    {
      if (is_file(base_path("storage/app/".$thumb))) {
        $img = \Image::make(base_path("storage/app/".$thumb))->resize($size[0], $size[1]);
        $img->save(base_path("storage/app/".$thumb));
        return $img;
      }
      return false;
    }
}

if (!function_exists('getFile')) {
  function getFile($pathFile) {
    $filename = end(explode("/", $pathFile));
    // header('Content-Type: text/csv; charset=utf-8');
    // header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    readfile(base_path($pathFile));
  }
}

if (!function_exists("inputForm")) {
    function inputForm($data = array()) {
        $html = "";
        $class = isset($data['class']) ? $data['class'] : "";
        $type = isset($data['type']) ? $data['type'] : "";
        $name = isset($data['name']) ? $data['name'] : "";
        $value = isset($data['value']) ? $data['value'] : "";
        $id = isset($data['id']) ? $data['id'] : "";
        $lable = isset($data['lable']) ? $data['lable'] : "";
        $tag = isset($data['tag']) ? $data['tag'] : "input";
        $editor = isset($data['editor']) ? $data['editor'] : false;
        $attribute = isset($data['attribute']) ? $data['attribute'] : "";
        $default_data = isset($data['data']) ? $data['data'] : (object) array(['id' => 1, 'name' => "No Data"]);
        $html .= "<div class=\"form-group\">";
        $html .= "<lable class=\"form-control-label\">$lable</lable>";
        switch ($tag) {
            case 'input':
                $html .= "<input type=\"$type\" name=\"$name\" value=\"$value\" id=\"$id\" class=\"form-control $class\" $attribute placeholder=\"$lable\" />";
                break;
            case 'textarea':
                $html .= "<textarea class=\"form-control $class\" name=\"$name\" id=\"$id\" placeholder=\"$lable\">$value</textarea>";
                break;
            case 'select':
                $html .= "<select $attribute name=\"$name\" data-plugin=\"select2\" class=\"form-control\">";
                foreach ($default_data as $item) {
                    $option_value = $item->id;
                    $option_name = $item->name;
                    $selected = "";
                    if (is_array($value) && in_array($option_value, $value)) {
                        $selected = "selected";
                    } elseif($option_value == $value) {
                        $selected = "selected";
                    }
                    $html .= "<option value=\"$option_value\" $selected>$option_name</option>";
                }
                $html .= "</select>";
                break;
            case 'checkbox':
                $html .= "<div class=\"row\">";
                foreach ($default_data as $item) {
                    $option_value = $item->id;
                    $option_name = $item->name;
                    $checked = "";
                    if (is_array($value) && in_array($option_value, $value)) {
                        $checked = "checked";
                    }
                    $html .= "<div class=\"col-md-4 col-sm-6 text-left\">";
                    $html .= "<div class=\"checkbox-custom checkbox-default checkbox-inline\">";
                    $html .= "<input type=\"checkbox\" id=\"$id$option_value\" name=\"$name\" value=\"$option_value\" $checked autocomplete=\"off\">";
                    $html .= "<label for=\"$id$option_value\">$option_name</label>";
                    $html .= "</div>";
                    $html .= "</div>";
                }
                $html .= "</div>";
                break;
            case 'radio':
                $html .= "<div class=\"row\">";
                foreach ($default_data as $item) {
                    $option_value = $item->id;
                    $option_name = $item->name;
                    $checked = "";
                    if (is_array($value) && in_array($option_value, $value)) {
                        $checked = "checked";
                    }
                    $html .= "<div class=\"col-md-4 col-sm-6 text-left\">";
                    $html .= "<div class=\"radio-custom radio-default radio-inline\">";
                    $html .= "<input type=\"radio\" id=\"$id$option_value\" name=\"$name\" value=\"$option_value\" $checked autocomplete=\"off\">";
                    $html .= "<label for=\"$id$option_value\">$option_name</label>";
                    $html .= "</div>";
                    $html .= "</div>";
                }
                $html .= "</div>";
                break;
            case 'tags':
                $html .= "<input type=\"text\" class=\"form-control\" data-plugin=\"tokenfield\" name=\"$name\" value=\"$value\"/>";
                break;
            case "image":
                $currentImage = url('global')."/photos/placeholder.png";
                if ($value) {
                  $currentImage = getImage($value);
                }
                $html .= "<input type=\"$type\" name=\"$name\" id=\"$id\" data-plugin=\"dropify\" data-height=\"300\" data-default-file=\"$currentImage\"/>";
                break;
            case 'file':
              $html .= "<input type=\"$type\" name=\"$name\" id=\"$id\" class=\"form-control\" />";
              if ($value) {
                 $html .= "<div class=\"block-help\"><a href=\"".getImage($value)."\" target=\"_blank\">".getImage($value)."</a></div>";
              }
              break;
            case "currency":
                $html .= "<input type=\"$type\" class=\"form-control $class\" name=\"$name\" $attribute id=\"$id\" data-plugin=\"currency\"
                    data-pattern=\"$value\" />";
                $html .= "<p class=\"text-help\">$999,999,999,999,999.99</p>";
                break;
            case "percent":
                $html .= "<input type=\"$type\" class=\"form-control $class\" name=\"$name\" $attribute id=\"$id\" data-plugin=\"currency\"
                    data-pattern=\"$value\" />";
                $html .= "<p class=\"text-help\">99.99%</p>";
                break;
            case 'date':
                $html .= "<div class=\"input-group\">
                    <span class=\"input-group-addon\">
                      <i class=\"icon wb-calendar\" aria-hidden=\"true\"></i>
                    </span>
                    <input type=\"$type\" class=\"form-control $class\" name=\"$name\" value=\"$value\" $attribute data-plugin=\"datepicker\">
                  </div>";
              break;
            default:
                # code...
                break;
        }
        $html .= "</div>";
        return $html;
    }
}

if (!function_exists("checkRoles")) {
    function checkRoles(){
        if(Auth::check() && in_array(Auth::user()->roles, config("constant.roles")))
            return true;
        return false;
    }
}

if (!function_exists('displayItem')) {
  function displayItem($key, $item)
  {
    switch ($key) {
      case 'image':
        if ($item->$key) {
          return "<img src=\"".getImage($item->$key)."\" class=\"img-thumbnail\">";
        }
        return "<div style=\"font-size: 66px;text-align: center;width: 100%;\"><i class=\"icon wb-image\"></i></div>";
        break;
      case 'is_active':
        $icon = "icon wb-minus-circle";
        $class = "danger";
        if ($item->$key == 1) {
          $icon = "icon wb-check";
          $class = "success";
        }
        return "<a style=\"display: block;\" href=\"javascript:void(0);\" class=\"btn btn-sm btn-icon btn-pure btn-$class on-default on-status\"
                      data-toggle=\"tooltip\" data-original-title=\"Status\"><i class=\"$icon\" aria-hidden=\"true\"></i></a>";
        break;
      case 'file':
        return "<a style=\"display: block;\" href=\"".getImage($item->file)."\" class=\"btn btn-sm btn-icon btn-pure on-default on-status\"
                      data-toggle=\"tooltip\" data-original-title=\"File\"><i class=\"icon wb-down\" aria-hidden=\"true\"></i> Download</a>";
        break;
      case 'categories_id':
        return \Models\Categories::find($item->$key)->name;
        break;
      case 'users_id':
        if ($item->$key) {
          return \Models\Users::find($item->$key)->name;
        }
        return \Models\Users::find(1)->name;
        break;
      case 'status':
        $status = [
          0 => [
            'class' => "warning",
            'icon' => "icon wb-time",
            'name' => "Đang Chờ"
          ],
          1 => [
            'class' => "info",
            'icon' => "icon wb-check-mini",
            'name' => "Phát Hành"
          ],
          2 => [
            'class' => "success",
            'icon' => "icon wb-check",
            'name' => "Thanh Toán"
          ],
          3 => [
            'class' => "danger",
            'icon' => "icon wb-info-circle",
            'name' => "Đã Rút Lại"
          ],
          4 => [
            'class' => "dark",
            'icon' => "icon wb-warning",
            'name' => "Từ Chối"
          ]
        ];
        return "<button type=\"button\" class=\"btn btn-outline btn-".$status[$item->$key]['class']."\"><i class=\"".$status[$item->$key]['icon']."\" aria-hidden=\"true\"></i> ".$status[$item->$key]['name']."</button>";
        break;
      default:
        return $item->$key;
        break;
    }
  }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationship_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->index();
            $table->integer('child_id')->index();
            $table->integer('status')->default(0);
            $table->integer('position')->default(0);
            $table->string('code')->default('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationship_categories');
    }
}

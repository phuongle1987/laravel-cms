<?php

namespace Phuongle\CMS\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRolesUsers
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        dd($request);
        return $next($request);
    }
}

<?php

namespace Phuongle\CMS\Models;

// use Illuminate\Database\Eloquent\Model;
use Moloquent\Eloquent\Model;
use Moloquent\Eloquent\SoftDeletes;

class ProductsCategory extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}

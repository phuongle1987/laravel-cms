<?php

namespace Phuongle\CMS;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ProductsModulesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        //
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadRoutesFrom(__DIR__.'/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views/products', 'products');
        $this->publishes([
            __DIR__.'/config/constant.php' => config_path('constant.php'),
            __DIR__.'/resources/views/products' => resource_path('views/vendor/products'),
        ]);
        $this->publishes([
            __DIR__.'/assets' => public_path('products'),
        ], 'public');
        $router->aliasMiddleware('author', 'Phuongle\CMS\Middleware\CheckRolesUsers');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}

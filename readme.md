## About Module

This is module cms with laravel framework

# Install Composer

[Composer](https://getcomposer.org/) by running the following from your project
root:

    $ composer require phuongledev/laravel-cms @dev

# Install in config

Add the service provider in config/app.php

	$ Phuongle\CMS\CMSModulesProvider::class,

# Pushlish vendor module

Run command line to publish vendor

	$ php artisan vendor:publish

	$ php artisan vendor:publish --tag=public --force [publish assets]
## Reporting Issues

Please use the following form to report any issues:


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
